import { WebResourceInterface } from '../interfaces/web-resource.interface';

export class WebResourceModel implements WebResourceInterface {
  /**
   * Resource domain
   * @type { String }
   */
  public readonly domain: string;

  /**
   * Path within domain
   * @type { String }
   */
  public readonly path: string;

  public constructor(params: WebResourceInterface = {} as WebResourceInterface) {
    this.domain = params.domain;
    this.path = params.path;
  }
}
