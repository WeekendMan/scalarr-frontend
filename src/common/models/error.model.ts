import { ErrorInterface } from '../interfaces/error.interface';

export class ErrorModel<E, D> implements ErrorInterface<E, D> {
  /**
   * Description for the error that should be set manually
   * @type { String }
   */
  public readonly description: string;

  /**
   * Error that we get from some hendler and want to return too
   * @type { E }
   */
  public readonly error: E;

  /**
   * Any data that we need to return with the error. Params, state, etc...
   * @type { D }
   */
  public readonly data: D;

  public constructor(params: ErrorInterface<E, D> = {} as ErrorInterface<E, D>) {
    this.description = params.description || 'Unexpected error';
    this.error = params.error;
    this.data = params.data;
  }
}
