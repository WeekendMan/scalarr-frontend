import { AlertModalParamsInterface } from '../../interfaces/modal/alert-modal-params.interface';
import { ModalModel } from './modal.model';

export class AlertModalModel extends ModalModel implements AlertModalParamsInterface {
  public readonly close: Function;

  public constructor(params: AlertModalParamsInterface = {} as AlertModalParamsInterface) {
    super(params);

    if (!!params.close) {
      this.close = params.close;
    }
  }
}
