import { ConfirmModalParamsInterface } from '../../interfaces/modal/confirm-modal-params.interface';
import { AlertModalModel } from './alert-modal.model';

export class ConfirmModalModel extends AlertModalModel implements ConfirmModalParamsInterface {
  /**
   * Handler for the "accept" button
   * @type { Function }
   */
  public readonly resolve: Function;

  /**
   * Handler for the "decline" button
   * @type { Function }
   */
  public readonly reject: Function;

  public constructor(params: ConfirmModalParamsInterface = {} as ConfirmModalParamsInterface) {
    super(params);

    this.resolve = params.resolve;

    if (params.reject instanceof Function) {
      this.reject = params.reject;
    }
  }
}
