import { CommonModalParamsInterface } from '../../interfaces/modal/common-modal-params.interface';

export class ModalModel implements CommonModalParamsInterface {
  /**
   * Title for the modal block
   * @type { String }
   */
  public readonly title: string;

  /**
   * Body for the modal block
   * @type { String }
   */
  public readonly body: string;

  public constructor(params: CommonModalParamsInterface = {} as CommonModalParamsInterface) {
    this.title = params.title;
    this.body = params.body;
  }
}
