import { Routes } from '@angular/router';
import { BaseResolver } from '../resolvers/base.resolver';
import { BaseComponent } from '../../app/base/base.component';
import { HomePageComponent } from '../../app/home-page/home-page.component';
import { NotFoundComponent } from '../../app/not-found/not-found.component';
import { SyncHtmlLoaderComponent } from '../../app/html-loader/sync/sync-html-loader.component';
import { AsyncHtmlLoaderComponent } from '../../app/html-loader/async/async-html-loader.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: BaseComponent,
    resolve: { check: BaseResolver },
    children: [
      {
        path: '',
        component: HomePageComponent,
        children: [
          {
            path: 'sync',
            component: SyncHtmlLoaderComponent
          },
          {
            path: 'async',
            component: AsyncHtmlLoaderComponent
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
