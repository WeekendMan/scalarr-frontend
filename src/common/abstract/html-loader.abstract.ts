import { HttpErrorResponse } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { ModalService } from '../services/modal.service';
import { HTMLLoaderService } from '../services/html-loader.service';
import { ErrorModel } from '../models/error.model';
import { AlertModalModel } from '../models/modal/alert-modal.model';
import { WebResourceModel } from '../models/web-resource.model';

export abstract class HTMLLoaderAbstract {
  /**
   * Content loaded from URLs stored in "urls" property
   * @type { String[] }
   */
  public html: DocumentFragment[];

  /**
   * Extended class should implement this property
   * @type { String[] }
   */
  protected abstract urls: WebResourceModel[];

  protected constructor(protected readonly modalService: ModalService,
                        protected readonly htmlLoaderService: HTMLLoaderService) {}

  /**
   * Method loads HTML from URLs in "urls" property and saves results in "html" property
   * @param sync { Boolean }
   * @async
   */
  protected load(sync: boolean = true): void {
    this.html = new Array(this.urls.length);

    if (sync) {// We'll get content for all sources in the same time
      this.syncLoader();
    } else {// We'll get content for all sources asynchronously
      this.asyncLoader();
    }
  }

  /**
   * Async loader for URLs
   * @returns { void }
   */
  private syncLoader(): void {
    // Concatenating the Observables
    forkJoin(
      this
        .urls
        .map<Observable<DocumentFragment>>(
          (resource: WebResourceModel): Observable<DocumentFragment> => this.htmlLoaderService.get(resource.domain, resource.path)
        )
    ).subscribe(
      (html: DocumentFragment[]): void => {// HTML for given URLs in the same order
        this.html = [ ... html ];
      },
      (error: ErrorModel<HttpErrorResponse, string>): void => {// Somethings is wrong
        console.error(`HTMLLoaderAbstract->load: ${error.description}`, error);
        this.modalService.showAlert(new AlertModalModel({
          title: 'Error!',
          body: error.description
        }));
      }
    );
  }

  /**
   * Sync loader for URLs
   * @returns { void }
   */
  private asyncLoader(): void {
    this.urls.forEach((resource: WebResourceModel, index: number): void => {// Taking each URL
      this.htmlLoaderService.get(resource.domain, resource.path).subscribe(// Sending request to get the HTML
        (html: DocumentFragment): void => {// Successful request, using the index to keep the order
          this.html[index] = html;
        },
        (error: ErrorModel<HttpErrorResponse, string>): void => {// Something is wrong
          console.error(`HTMLLoaderAbstract->load: ${error.description}`, error);
          this.modalService.showAlert(new AlertModalModel({
            title: 'Error!',
            body: error.description
          }));
        }
      );
    });
  }
}
