export interface CommonModalParamsInterface {
  /**
   * Title for the modal block
   * @type { String }
   */
  title: string;

  /**
   * Body for the modal block
   * @type { String }
   */
  body: string;
}
