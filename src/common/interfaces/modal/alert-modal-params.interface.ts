import { CommonModalParamsInterface } from './common-modal-params.interface';

export interface AlertModalParamsInterface extends CommonModalParamsInterface {
  /**
   * Handler for the "close" event
   * @type { Function }
   */
  close?: Function;
}
