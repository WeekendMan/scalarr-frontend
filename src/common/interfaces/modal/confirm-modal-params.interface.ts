import { AlertModalParamsInterface } from './alert-modal-params.interface';

export interface ConfirmModalParamsInterface extends AlertModalParamsInterface {
  /**
   * Handler for the "accept" button
   * @type { Function }
   */
  resolve: Function;

  /**
   * Handler for the "decline" button
   * @type { Function }
   */
  reject?: Function;
}
