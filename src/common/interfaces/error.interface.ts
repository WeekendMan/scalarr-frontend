export interface ErrorInterface<E, D> {
  /**
   * Human-readable description of the error, will be set by server
   * @type { String }
   */
  description: string;

  /**
   * Some data that server can return with error to do something
   * @type { D }
   */
  data: D;

  /**
   * Some error object that we can pass to some controller. For example HttpErrorResponse in the http services
   * @type { E }
   */
  error: E;
}
