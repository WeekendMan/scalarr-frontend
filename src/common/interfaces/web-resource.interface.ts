export interface WebResourceInterface {
  /**
   * Resource domain
   * @type { String }
   */
  domain: string;

  /**
   * Path within domain
   * @type { String }
   */
  path: string;
}
