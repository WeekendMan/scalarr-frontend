import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

export class ModalBase {
  /**
   * Title for the modal window. Will be given from some component via the modal service
   * @type { String }
   */
  public title: string;

  /**
   * Text that we want to show to user. Will be given from some component via the modal service
   * @type { String }
   */
  public body: string;

  /**
   * Callback that will be called when modal window will be hidden
   * @type { Function }
   */
  public close: Function;

  /**
   * Callback for the "Accept" button that will be given from the component that emitted this alert
   * @type { Function }
   */
  public resolve: Function;

  /**
   * Callback for the "Decline" button that may be given (optional) from the component that emitted this alert
   * @type { Function }
   */
  public reject: Function;

  /**
   * Subscription to the "onHidden" event of the ngxBootstrap
   * @type { Subscription }
   */
  protected onHiddenSubscription: Subscription;

  protected constructor(protected ngbActiveModal: NgbActiveModal) {}

  /**
   * Method does the common actions for the onResolve and onReject logic
   * @returns { void }
   */
  public onClose(): void {
    // Hiding the modal window
    this.ngbActiveModal.dismiss();
  }

  /**
   * Method will execute given resolve callback and close the modal window
   * @returns { void }
   */
  public onResolve(): void {
    // Executing given callback if exists
    if (!!this.resolve) {
      this.resolve();

      // Manually removing the callbacks here to prevent double execution
      delete this.resolve;
      delete this.reject;
    } else {
      console.error('ConfirmModalComponent->resolve: No callback for "resolve" action', this.onResolve);
    }

    this.onClose();
  }

  /**
   * Method will execute given reject callback and close the modal window
   * @returns { void }
   */
  public onReject(): void {
    this.onClose();
  }

  /**
   * Method that should destroy all properties.
   * @returns { void }
   */
  protected onHiddenEventHandler(): void {
    // Executing given callback if exists
    if (!!this.reject) {
      this.reject();
    }

    if (!!this.close) {
      this.close();
    }

    // Removing given values
    this.title = '';
    this.body = '';
    delete this.onClose;
    delete this.resolve;
    delete this.reject;

    // Unsibscribing from this subscription because component will be inactive
    if (!!this.onHiddenSubscription) {
      this.onHiddenSubscription.unsubscribe();
    }
  }
}
