import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalBase } from './modal.base';

export class AlertModalBase extends ModalBase {
  public constructor(protected ngbActiveModal: NgbActiveModal) {
    super(ngbActiveModal);
  }
}
