import { LayoutService } from '../../services/layout.service';
import { BaseResolver } from '../base.resolver';

class TestLayoutService extends LayoutService {
  public constructor(public doneFn: DoneFn) {
    super();
  }

  public unlockWindow(): void {
    this.doneFn();
  }
}

describe(
  'Base resolver should call unlockWindow method of LayoutService after async logic',
  (): void => {
    // Todo: Test should be updated with real logic in "resolve" method

    it (
      'Method calls "unlockWindow" method of "TestLayoutService',
      (doneFn: DoneFn): void => {
        const testLayoutService: LayoutService = new TestLayoutService(doneFn);
        const baseResolver: BaseResolver = new BaseResolver(testLayoutService);
        let result: boolean;

        baseResolver.resolve().subscribe(
          (): void => {
            result = true;
          },
          (): void => {
            result = false;
          },
          (): void => {
            expect(result).toBeTruthy();
          }
        );
      }
    );
  }
);
