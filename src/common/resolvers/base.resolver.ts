import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, interval, Observer } from 'rxjs';
import { LayoutService } from '../services/layout.service';

@Injectable({ providedIn: 'root' })

export class BaseResolver implements Resolve<void> {
  public constructor(private layoutService: LayoutService) {}

  /**
   * Method implemented due to the interface contract
   * @returns { Observable<Boolean> }
   * @async
   */
  public resolve(): Observable<void> {
    // Primary security check will be here. For now we just emitting latency via timer
    return Observable.create((observer: Observer<void>): void => {
      interval(2000).subscribe((): void => {
        this.layoutService.unlockWindow();
        observer.next(void 0);
        observer.complete();
      });
    });
  }
}
