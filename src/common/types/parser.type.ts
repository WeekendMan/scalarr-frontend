export type ParserType<T> = ((response: string) => T) | null;
