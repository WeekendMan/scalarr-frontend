import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class HttpInterceptorService implements HttpInterceptor {
  /**
   * Method implemented due to the interface contract
   * @param request { HttpRequest<Any> }
   * @param next { HttpHandler }
   * @returns { Observable<HttpEvent<Any>> }
   */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Cloning the request to add all request params that we need due to the Angular docs
    // Link: https://angular.io/guide/http#intercepting-all-requests-or-responses
    const clonedRequest: HttpRequest<any> = request.clone({
      withCredentials: true,
      responseType: 'text'
    });

    // Returning the changed request
    return next.handle(clonedRequest);
  }
}
