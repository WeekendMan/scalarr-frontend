import { EventEmitter, Injectable } from '@angular/core';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { KeyValueInterface } from '../interfaces/key-value.interface';
import { AlertModalModel } from '../models/modal/alert-modal.model';
import { ConfirmModalModel } from '../models/modal/confirm-modal.model';
import { AlertModalComponent } from '../../app/modal/alert/alert.component';
import { ConfirmModalComponent } from '../../app/modal/confirm/confirm.component';

@Injectable({ providedIn: 'root' })

export class ModalService {
  /**
   * Event for the subscribers that need to be notified about the hiding the modal window
   * @type { EventEmitter<void> }
   */
  public readonly onHidden: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Reference to the active modal window.
   * @type { NgbModalRef }
   */
  protected ngbModalRef: NgbModalRef;

  /**
   * Options for the Modal module of Bootstrap
   * @type { NgbModalOptions }
   */
  protected readonly options: NgbModalOptions = {
    centered: true,
    beforeDismiss: (): boolean => {
      // Event for the subscribers that modal window is about to close
      this.onHidden.emit();

      if (
        !!this.ngbModalRef &&
        !!this.ngbModalRef.componentInstance &&
        !!this.ngbModalRef.componentInstance.onHiddenEventHandler
      ) {
        this.ngbModalRef.componentInstance.onHiddenEventHandler();
      }

      // Removing the previous reference
      delete this.ngbModalRef;

      // Close the window
      return true;
    }
  };

  public constructor(protected readonly ngbModal: NgbModal) {}

  /**
   * Method for the app entities that want to show the alert window with some context
   * @param params { AlertModalModel }
   * @returns { void }
   */
  public showAlert(params: AlertModalModel): void {
    this.ngbModalRef = this.ngbModal.open(AlertModalComponent, this.options);
    this.setModalParams(params);
  }

  /**
   * Method for the app entities that want to show the confirm window with some context
   * @param params { ConfirmModalModel }
   * @returns { void }
   */
  public showConfirm(params: ConfirmModalModel): void {
    this.ngbModalRef = this.ngbModal.open(ConfirmModalComponent, this.options);
    this.setModalParams(params);
  }

  /**
   * Method for showing the modal window that will contain some component's layout
   * @param component { Any }
   * @param params { KeyValueInterface<Any> }
   * @param options { KeyValueInterface<Any> }
   * @returns { void }
   */
  public showModal(component: any, params: KeyValueInterface<any> = {}, options: KeyValueInterface<any> = {}): void {
    this.ngbModalRef = this.ngbModal.open(component, { ... this.options, ... options });
    this.setModalParams(params);
  }

  /**
   * Method sets the params for the modal components by reference
   * @param params { AlertModalModel | ConfirmModalModel | KeyValueInterface<Any> }
   * @returns { void }
   */
  protected setModalParams(params: AlertModalModel | ConfirmModalModel | KeyValueInterface<any>): void {
    // Checking the existence of the property
    if (!!this.ngbModalRef && typeof params === 'object') {
      for (const key of Object.keys(params)) {
        this.ngbModalRef.componentInstance[key] = params[key];
      }
    }
  }
}
