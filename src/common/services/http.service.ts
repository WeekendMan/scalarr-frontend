import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observer, Observable } from 'rxjs';
import { ParserType } from '../types/parser.type';
import { KeyValueInterface } from '../interfaces/key-value.interface';
import { HTTPHelper } from '../helpers/http.helper';
import { ErrorHelper } from '../helpers/error.helper';
import { JSONHelper } from '../helpers/json.helper';
import { ErrorModel } from '../models/error.model';

@Injectable({ providedIn: 'root' })

export class HTTPService {
  public constructor(protected readonly http: HttpClient) {}

  /**
   * Wrapper above the Angular http.get method.
   * @param url { String }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public get<T>(
    url: string,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.get.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Wrapper above the Angular http.delete method.
   * @param url { String }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public delete<T>(
    url: string,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.delete.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Wrapper above the Angular http.head method.
   * @param url { String }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public head<T>(
    url: string,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.head.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Wrapper above the Angular http.post method.
   * @param url { String }
   * @param body { KeyValueInterface<Any> }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public post<T>(
    url: string,
    body: KeyValueInterface<any>,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.post.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`,
        body
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Wrapper above the Angular http.put method.
   * @param url { String }
   * @param body { KeyValueInterface<Any> }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public put<T>(
    url: string,
    body: KeyValueInterface<any>,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.put.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`,
        body
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Wrapper above the Angular http.patch method.
   * @param url { String }
   * @param body { KeyValueInterface<Any> }
   * @param parser { ParserType<T> }
   * @param queryParams { KeyValueInterface<any> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public patch<T>(
    url: string,
    body: KeyValueInterface<any>,
    parser: ParserType<T> = null,
    queryParams: KeyValueInterface<any> = {},
    securityCheck: boolean = true
  ): Observable<T> {
    return this.sendRequest<T>(
      this.http.patch.bind(
        this.http,
        `${url}${HTTPHelper.createQueryString(queryParams)}`,
        body
      ),
      parser,
      securityCheck
    );
  }

  /**
   * Method executes the request using the first param as request method
   * @param method { () => Observable<String> }
   * @param parser { ParserType<T> }
   * @param securityCheck { Boolean }
   * @returns { Observable<T> }
   * @async
   */
  public sendRequest<T>(method: () => Observable<string>, parser: ParserType<T>, securityCheck: boolean): Observable<T> {
    return Observable.create((observer: Observer<T | string>): void => {
      method().subscribe(
        (response: string = ''): void => {
          if (!!response && (!securityCheck || JSONHelper.isSafeJSON(response))) {// Checking JSON safety if response exists
            observer.next(
              (!!parser)
                ? parser(response)
                : response
            );
          } else {
            observer.error(new ErrorModel<Error, string>({
              description: `JSON from server contains the restricted chars and won't be executed`,
              error: new Error('Unsafe JSON from API'),
              data: response
            }));
          }
        },
        (error: HttpErrorResponse): void => observer.error(ErrorHelper.createErrorFromHttpErrorResponse<void>(error)),
        (): void => observer.complete()
      );
    });
  }
}
