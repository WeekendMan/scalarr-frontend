import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { HTTPService } from './http.service';
import { HTTPHelper } from '../helpers/http.helper';
import { HTMLHelper } from '../helpers/html.helper';
import { ErrorModel } from '../models/error.model';

@Injectable({ providedIn: 'root' })

export class HTMLLoaderService {
  public constructor(private readonly httpService: HTTPService) {}

  /**
   * Method loads HTML from given URL
   * @param domain { String }
   * @param path { String }
   * @returns { Observable<String> }
   * @async
   */
  public get(domain: string, path: string): Observable<DocumentFragment> {
    return Observable.create((observer: Observer<DocumentFragment>): void => {
      this.httpService.post<DocumentFragment>(
        HTTPHelper.createApiURL('proxy'),
        { url: `${domain}${path}` },
        HTMLHelper.getPageBodyContent,
        {},
        false
      ).subscribe(
        (html: DocumentFragment): void => observer.next(html),
        (error: ErrorModel<HttpErrorResponse, string>): void => observer.error(error),
        (): void => observer.complete()
      );
    });
  }
}
