import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment';
import { HTMLLoaderService } from '../html-loader.service';
import { ErrorModel } from '../../models/error.model';

describe(
  'HTMLLoaderService',
  (): void => {
    let injector: TestBed;
    let httpMock: HttpTestingController;
    let htmlLoaderService: HTMLLoaderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ HttpClientTestingModule ],
        providers: [ HTMLLoaderService ]
      });
      injector = getTestBed();
      htmlLoaderService = injector.get(HTMLLoaderService);
      httpMock = injector.get(HttpTestingController);
    });

    afterEach(() => {
      httpMock.verify();
    });

    describe(
      '#get',
      (): void => {
        it(
          'Method "get" should return Observable and request should be resolved',
          (): void => {
            const domain: string = 'https://some-domain.com';
            const path: string = '/some-path';
            htmlLoaderService.get(domain, path).subscribe(
              (fragment: DocumentFragment): void => {
                expect(!!fragment.getElementById && !!fragment.querySelectorAll).toBeTruthy();
              }
            );
            const req: TestRequest = httpMock.expectOne(`${environment.apiURL}proxy`);
            expect(req.request.method).toBe('POST');
            req.flush(`
              <!doctype html>
              <html>
                <head>
                  <style>body {font: 12px Arial;}</style>
                  <script>alert(1);</script>
                </head>
                <body>
                  <script>alert(2);</script>
                  <style>p {margin-bottom: 15px;}</style>
                  <p>Some Paragraph</p>
                </body>
              </html>
            `);
          }
        );

        it(
          'Method "get" should return Observable and request should be failed',
          (doneFn: DoneFn): void => {
            const domain: string = 'https://some-domain.com';
            const path: string = '/some-path';

            const mockError: ErrorModel<HttpErrorResponse, string> = new ErrorModel<HttpErrorResponse, string>({
              description: 'Test Description',
              data: 'SomeTestValue',
              error: new HttpErrorResponse({
                error: new Error('Test Error'),
                headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
                status: 400,
                statusText: 'Incorrect Request Body',
                url: 'https://test-url.com'
              })
            });

            htmlLoaderService.get(domain, path).subscribe(
              (): void => fail(`Observable shouldn't be successful`),
              (): void => doneFn()
            );
            const req: TestRequest = httpMock.expectOne(`${environment.apiURL}proxy`);
            expect(req.request.method).toBe('POST');
            req.flush({}, mockError.error);
          }
        );
      }
    );
  }
);
