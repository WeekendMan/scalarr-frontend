import { EventEmitter, Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';

@Injectable({ providedIn: 'root' })

export class LayoutService {
  /**
   * Event that will be emitted when document.body gets the click event.
   * Generic param is the element that started the event that bubbled to the body element.
   * @type { EventEmitter<HTMLElement> }
   */
  public readonly onBodyClick: EventEmitter<HTMLElement> = new EventEmitter<HTMLElement>();

  /**
   * Event will be emitted when user press some key
   * @type { EventEmitter<Event> }
   */
  public readonly onBodyKeydown: EventEmitter<KeyboardEvent> = new EventEmitter<KeyboardEvent>();

  /**
   * Event will be emitted when user release some key
   * @type { EventEmitter<KeyboardEvent> }
   */
  public readonly onBodyKeyup: EventEmitter<KeyboardEvent> = new EventEmitter<KeyboardEvent>();

  /**
   * HTML body element saved to the property
   * @type { HTMLElement }
   */
  protected readonly body: HTMLElement = document.body;

  public constructor() {
    fromEvent<MouseEvent>(document.body, 'click').subscribe((event: MouseEvent): void => {
      this.onBodyClick.emit(event.target as HTMLElement);
    });

    fromEvent<KeyboardEvent>(document.body, 'keydown').subscribe((event: KeyboardEvent): void => {
      this.onBodyKeydown.emit(event);
    });

    fromEvent<KeyboardEvent>(document.body, 'keyup').subscribe((event: KeyboardEvent): void => {
      this.onBodyKeyup.emit(event);
    });
  }

  /**
   * Method locks the whole window content.
   * It shows the layout with the spinner above the all another layouts.
   * @returns { void }
   */
  public lockWindow(): void {
    this.body.classList.add('init');
  }

  /**
   * Method does the reverse action of the lockWindow method.
   * @returns { void }
   */
  public unlockWindow(): void {
    this.body.classList.remove('init');
  }
}
