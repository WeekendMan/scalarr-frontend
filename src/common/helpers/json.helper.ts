import { KeyValueInterface } from '../interfaces/key-value.interface';

export class JSONHelper {
  /**
   * Method wraps JSON parse action into the try/catch to have this logic in the one place
   * @param json { String }
   * @returns { Any }
   */
  public static parse(json: string): any {
    try {
      return JSON.parse(json);
    } catch (e) {
      return {};
    }
  }

  /**
   * Method wraps JSON stringify action into the try/catch to have this logic in the one place
   * @param data { String | KeyValueInterface<Any> }
   * @returns { String }
   */
  public static stringify(data: string | KeyValueInterface<any>): string {
    try {
      return JSON.stringify(data);
    } catch (e) {
      return '';
    }
  }

  /**
   * Method returns true if given string is valid JSON.
   * If not, false will be returned
   * @param json { String }
   * @returns { Boolean }
   */
  public static isSafeJSON(json: string): boolean {
    return (/^[\],:{}\s]*$/).test(
      json
        .replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
        .replace(/(?:^|:|,)(?:\s*\[)+/g, '')
    );
  }
}
