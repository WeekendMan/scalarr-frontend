export class HTMLHelper {
  public static getPageBodyContent(rawHTML: string): DocumentFragment {
    // Inserting raw HTML string to have the possibility to manage elements
    const html: HTMLElement = document.createElement('div');
    html.innerHTML = rawHTML;

    // Looking for the body element inside
    const body: HTMLBodyElement | null = html.querySelector('body');

    // If there is no body, we'll take child nodes from root element
    const source: HTMLElement = body || html;

    // Removing scripts and styles
    Array.from<Node>(source.querySelectorAll<Element>('style, script')).forEach((el: Node): void => {
      if (!!el.parentElement) {
        el.parentElement.removeChild(el);
      }
    });

    // Creating DocumentFragment and appending child nodes there
    const content: DocumentFragment = document.createDocumentFragment();
    content.append(... Array.from<Element>(source.children));

    return content;
  }
}
