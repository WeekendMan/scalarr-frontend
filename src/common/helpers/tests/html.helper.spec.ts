import { HTMLHelper } from '../html.helper';

describe(
  'HTMLHelper',
  (): void => {
    const valueToSearch: string = 'Value To Search';
    const html: string = `
      <!doctype html>
      <html>
        <head>
          <style>body {font: 12px Arial;}</style>
          <script>alert(1);</script>
        </head>
        <body>
          <script>alert(2);</script>
          <style>p {margin-bottom: 15px;}</style>
          <p>${valueToSearch}</p>
        </body>
      </html>
    `;
    const fragment: DocumentFragment = HTMLHelper.getPageBodyContent(html);
    const maybeScriptsAndStyles: NodeListOf<Element> = fragment.querySelectorAll<Element>('style, script, body');

    it(
      'All styles, scripts and body element should be removed',
      (): void => {
        expect(maybeScriptsAndStyles.length).toBe(0);
      }
    );
  }
);
