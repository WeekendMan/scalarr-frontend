import { JSONHelper } from '../json.helper';
import { KeyValueInterface } from '../../interfaces/key-value.interface';

describe(
  'JSONHelper',
  (): void => {
    // Object will be used multiple times to test JSON features
    const obj: KeyValueInterface<string> = { param1: 'value1', param2: 'value2' };

    it(
      'Method "parse" should generate filled plain JS Object from given valid JSON',
      (): void => {
        const parsed: KeyValueInterface<any> = JSONHelper.parse(JSON.stringify(obj));

        expect(typeof parsed).toBe('object');
        expect(Object.keys(parsed).length).toBe(Object.keys(obj).length);
        expect(parsed.param1).toBe(obj.param1);
        expect(parsed.param2).toBe(obj.param2);
      }
    );

    it(
      'Method "parse" should generate empty JS Object from given incorrect JSON as first param',
      (): void => {
        const incorrectJSON: string = '{ "param1" "value1", param2": "value2" }';
        const parsed: KeyValueInterface<any> = JSONHelper.parse(incorrectJSON);

        expect(typeof parsed).toBe('object');
        expect(Object.keys(parsed).length).toBe(0);
      }
    );

    it(
      'Method "isSafeJSON" should mark given JSON with XSS injection as invalid',
      (): void => {
        const jsonWithXSS: string = '{ "safeParam" "safeValue", "xssParam": alert(1) }';
        expect(JSONHelper.isSafeJSON(jsonWithXSS)).toBeFalsy();
      }
    );

    it(
      'Method "isSafeJSON" should mark given safe JSON as valid',
      (): void => {
        expect(JSONHelper.isSafeJSON(JSON.stringify(obj))).toBeTruthy();
      }
    );
  }
);
