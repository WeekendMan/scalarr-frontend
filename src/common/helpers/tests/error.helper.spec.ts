import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ErrorHelper } from '../error.helper';
import { ErrorModel } from '../../models/error.model';

describe(
  'ErrorHelper',
  (): void => {
    const httpErrorResponse: HttpErrorResponse = new HttpErrorResponse({
      error: new Error('Test Error'),
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      status: 400,
      statusText: 'Incorrect Request Body',
      url: 'https://test-url.com'
    });

    it(// Testing returned value from method
      'Method "createErrorFromHttpErrorResponse" should return ErrorModel with HttpErrorResponse in "error" propery',
      (): void => {
        const error: ErrorModel<HttpErrorResponse, void> = ErrorHelper.createErrorFromHttpErrorResponse(httpErrorResponse);

        expect(error.constructor).toBe(ErrorModel);
        expect(error.error.constructor).toBe(HttpErrorResponse);
      }
    );

    it(// Testing method that creates ErrorModel from HttpErrorResponse without data
      'Method "createErrorFromHttpErrorResponse" should return ErrorModel without data',
      (): void => {
        const error: ErrorModel<HttpErrorResponse, void> = ErrorHelper.createErrorFromHttpErrorResponse(httpErrorResponse);
        expect(error.data).toBe(undefined);
      }
    );

    it(// Testing method that creates ErrorModel from HttpErrorResponse with data
      'Method "createErrorFromHttpErrorResponse" should return ErrorModel with data',
      (): void => {
        const data: string = 'SomeStringValue';
        const error: ErrorModel<HttpErrorResponse, string> = ErrorHelper.createErrorFromHttpErrorResponse(httpErrorResponse, data);
        expect(error.data).toBe(data);
      }
    );
  }
);
