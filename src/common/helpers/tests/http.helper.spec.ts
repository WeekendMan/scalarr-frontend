import { environment } from '../../../../environments/environment';
import { HTTPHelper } from '../http.helper';

describe(
  'HTTPHelper',
  (): void => {
    it(
      'Method "createQueryString" should generate query string with params from JS Object given as first param',
      (): void => {
        const queryString: string = HTTPHelper.createQueryString({ param1: 'value1', param2: 'value2' });
        expect(queryString).toBe('?param1=value1&param2=value2');
      }
    );

    it(
      'Method "createQueryString" should generate empty string if empty JS Object given as first param',
      (): void => {
        const queryString: string = HTTPHelper.createQueryString({});
        expect(queryString).toBe('');
      }
    );

    it(
      'Method "createApiURL" should create valid request url',
      (): void => {
        const path: string = 'some-path';
        expect(HTTPHelper.createApiURL(path)).toBe(`${environment.apiURL}${path}`);
      }
    );
  }
);
