import { KeyValueInterface } from '../interfaces/key-value.interface';
import { environment } from '../../../environments/environment';

export class HTTPHelper {
  /**
   * Method creates query string from the given key => value structure
   * @param data { KeyValueInterface<Any> | Map<Any, Any> }
   * @returns { String }
   */
  public static createQueryString(data: KeyValueInterface<any>): string {
    let queryString: string = '';

    if (!!data) {
      const keys: any[] = Object.keys(data);

      for (let counter: number = 0; counter < keys.length; counter ++) {
        if (data[keys[counter]] !== null && data[keys[counter]] !== undefined) {
          if (data[keys[counter]] instanceof Array && !!data[keys[counter]].length) {
            for (let i: number = 0; i < data[keys[counter]].length; i ++) {
              queryString += `${keys[counter]}[]=${data[keys[counter]][i]}${i < data[keys[counter]].length - 1 ? '&' : ''}`;
            }

            queryString += (
              counter < keys.length - 1
                ? '&'
                : ''
            );
          } else if (data[keys[counter]] instanceof Date) {
            queryString += `${keys[counter]}=${data[keys[counter]].toISOString()}${counter < keys.length - 1 ? '&' : ''}`;
          } else if (
            typeof data[keys[counter]] === 'string' ||
            typeof data[keys[counter]] === 'number' ||
            typeof data[keys[counter]] === 'boolean'
          ) {// Primitives
            queryString += `${keys[counter]}=${data[keys[counter]]}${counter < keys.length - 1 ? '&' : ''}`;
          }
        }
      }

      if (!!queryString.length) {
        queryString = '?' + queryString;
      }
    }

    return queryString;
  }

  /**
   * Wrapper for creating URL to API
   * @param url { String }
   * @returns { String }
   */
  public static createApiURL(url: string): string {
    return `${environment.apiURL}${url}`;
  }
}
