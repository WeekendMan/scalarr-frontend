import { HttpErrorResponse } from '@angular/common/http';
import { KeyValueInterface } from '../interfaces/key-value.interface';
import { ErrorModel } from '../models/error.model';
import { JSONHelper } from './json.helper';
import { ErrorInterface } from '../interfaces/error.interface';

export class ErrorHelper {
  /**
   * Method creates the error model from given params.
   * We need this method since this action will be repeated a lot of times within the services
   * @param error { HttpErrorResponse }
   * @param data { T }
   * @returns { ErrorModel<T> }
   */
  public static createErrorFromHttpErrorResponse<T>(error: HttpErrorResponse, data?: T): ErrorModel<HttpErrorResponse, T> {
    try {// If we can parse server response
      const parsed: KeyValueInterface<any> = JSONHelper.parse(error.error);

      return new ErrorModel<HttpErrorResponse, T>({
        description: parsed.description || 'Unexpected error',
        error,
        data
      } as ErrorInterface<HttpErrorResponse, T>);
    } catch (e) {// If response doesn't have JSON data
      return new ErrorModel<HttpErrorResponse, T>(({
        description: 'Unexpected server error',
        error,
        data: e
      }));
    }
  }
}
