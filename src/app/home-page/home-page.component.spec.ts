import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HomePageComponent } from './home-page.component';
import { DebugElement, Directive, HostListener, Input } from '@angular/core';
import { By } from '@angular/platform-browser';

@Directive({ selector: '[routerLink]' })
export class RouterLinkStubDirective {
  @Input('routerLink')
  public linkParams: string | string[];

  public navigatedTo: string | string[];

  @HostListener('click')
  public onClick() {
    this.navigatedTo = this.linkParams;
  }
}

describe('HomePageComponent', () => {
  let fixture: ComponentFixture<HomePageComponent>;
  let compiled: HTMLElement;
  let linkDebugElements: DebugElement[];
  let linkDirectives: RouterLinkStubDirective[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        HomePageComponent,
        RouterLinkStubDirective
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePageComponent);
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
    linkDebugElements = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective));
    linkDirectives = linkDebugElements.map<RouterLinkStubDirective>(
      (e: DebugElement): RouterLinkStubDirective => e.injector.get(RouterLinkStubDirective)
    );
  }));

  it('Should create Home Component', (): void => {
    expect(compiled).toBeDefined();
  });

  it('Should render header', (): void => {
    expect(compiled.querySelector('header')).toBeTruthy();
  });

  it('Header should contain 3 links', (): void => {
    expect(linkDebugElements.length).toEqual(3);
    expect(linkDirectives[0].linkParams).toBe('/');
    expect(linkDirectives[1].linkParams).toBe('/sync');
    expect(linkDirectives[2].linkParams).toBe('/async');
  });
});
