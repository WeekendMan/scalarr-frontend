import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

@Component({
  templateUrl: './home-page.component.html',
  styleUrls: [ './home-page.component.scss' ]
})

export class HomePageComponent {
  constructor(private readonly title: Title) {
    this.title.setTitle(`Home Page | ${environment.appName}`);
  }
}
