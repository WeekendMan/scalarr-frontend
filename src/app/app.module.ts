import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ROUTES } from '../common/const/routes.const';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpInterceptorService } from '../common/services/http-interceptor';

import { AppComponent } from './app.component';
import { BaseComponent } from './base/base.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AlertModalComponent } from './modal/alert/alert.component';
import { ConfirmModalComponent } from './modal/confirm/confirm.component';
import { HTMLLoaderModule } from './html-loader/html-loader.module';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    NotFoundComponent,
    HomePageComponent,
    AlertModalComponent,
    ConfirmModalComponent
  ],
  entryComponents: [
    AlertModalComponent,
    ConfirmModalComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    HttpClientModule,
    HTMLLoaderModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    }
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}
