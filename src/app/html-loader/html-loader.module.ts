import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HTMLLoaderViewComponent } from './view/html-loader-view.component';
import { SyncHtmlLoaderComponent } from './sync/sync-html-loader.component';
import { AsyncHtmlLoaderComponent } from './async/async-html-loader.component';

@NgModule({
  declarations: [
    HTMLLoaderViewComponent,
    SyncHtmlLoaderComponent,
    AsyncHtmlLoaderComponent
  ],
  imports: [
    SharedModule
  ]
})

export class HTMLLoaderModule {}
