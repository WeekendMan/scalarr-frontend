import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-html-loader-view',
  templateUrl: './html-loader-view.component.html',
  styleUrls: [ './html-loader-view.component.scss' ]
})

export class HTMLLoaderViewComponent {
  /**
   * List of contents wrapped to DocunentFragment to display
   * @type { DocumentFragment[] }
   */
  @Input()
  public html: DocumentFragment[];
}
