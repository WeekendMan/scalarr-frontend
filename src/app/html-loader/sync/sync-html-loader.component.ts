import { Component } from '@angular/core';
import { HTMLLoaderAbstract } from '../../../common/abstract/html-loader.abstract';
import { ModalService } from '../../../common/services/modal.service';
import { HTMLLoaderService } from '../../../common/services/html-loader.service';
import { WebResourceModel } from '../../../common/models/web-resource.model';
import { environment } from '../../../../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  templateUrl: './sync-html-loader.component.html',
  styleUrls: [ './sync-html-loader.component.scss' ]
})

export class SyncHtmlLoaderComponent extends HTMLLoaderAbstract {
  /**
   * URLs to take content from
   * @type { String[] }
   */
  protected urls: WebResourceModel[] = [
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_basic_img'
    }),
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_lists_intro'
    }),
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_basic_paragraphs'
    })
  ];

  public constructor(protected readonly modalService: ModalService,
                     protected readonly htmlLoaderService: HTMLLoaderService,
                     private readonly title: Title) {
    super(modalService, htmlLoaderService);
    this.title.setTitle(`Sync Loading | ${environment.appName}`);
    this.load();
  }
}
