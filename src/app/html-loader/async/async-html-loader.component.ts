import { Component } from '@angular/core';
import { HTMLLoaderAbstract } from '../../../common/abstract/html-loader.abstract';
import { ModalService } from '../../../common/services/modal.service';
import { HTMLLoaderService } from '../../../common/services/html-loader.service';
import { WebResourceModel } from '../../../common/models/web-resource.model';
import { Title } from '@angular/platform-browser';
import { environment } from '../../../../environments/environment';

@Component({
  templateUrl: './async-html-loader.component.html',
  styleUrls: [ './async-html-loader.component.scss' ]
})

export class AsyncHtmlLoaderComponent extends HTMLLoaderAbstract {
  /**
   * URLs to take content from
   * @type { String[] }
   */
  protected urls: WebResourceModel[] = [
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_basic_img'
    }),
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_lists_intro'
    }),
    new WebResourceModel({
      domain: 'https://www.w3schools.com',
      path: '/html/tryit.asp?filename=tryhtml_basic_paragraphs'
    })
  ];

  public constructor(protected readonly modalService: ModalService,
                     protected readonly htmlLoaderService: HTMLLoaderService,
                     private readonly title: Title) {
    super(modalService, htmlLoaderService);
    this.title.setTitle(`Async Loading | ${environment.appName}`);
    this.load(false);
  }
}
