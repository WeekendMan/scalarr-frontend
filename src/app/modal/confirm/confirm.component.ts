import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalBase } from '../../../common/base/modal/confirm-modal.base';

@Component({
  templateUrl: './confirm.component.html',
  styleUrls: [ './confirm.component.scss' ]
})

export class ConfirmModalComponent extends ConfirmModalBase {
  public constructor(protected readonly ngbActiveModal: NgbActiveModal) {
    // Initing the parent class
    super(ngbActiveModal);
  }
}
