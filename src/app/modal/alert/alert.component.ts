import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertModalBase } from '../../../common/base/modal/alert-modal.base';

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert.component.html',
  styleUrls: [ './alert.component.scss' ]
})

export class AlertModalComponent extends AlertModalBase {
  public constructor(protected readonly ngbActiveModal: NgbActiveModal) {
    super(ngbActiveModal);
  }
}
