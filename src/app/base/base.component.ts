import { Component } from '@angular/core';

@Component({
  template: '<router-outlet></router-outlet>',
})

// Don't edit this component, it will be used only for including all app components to load logged user info before template rendering
export class BaseComponent {}
