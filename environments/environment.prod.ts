export const environment = {
  production: true,
  apiURL: '//localhost:9000/api/',
  appName: 'Scalarr'
};
